package com.example.lukaszwachowski.tensorflow.classifier.tensorflow

import android.graphics.Bitmap
import com.example.lukaszwachowski.tensorflow.classifier.COLOR_CHANNELS
import com.example.lukaszwachowski.tensorflow.classifier.Classifier
import com.example.lukaszwachowski.tensorflow.classifier.Result
import org.tensorflow.Graph
import org.tensorflow.contrib.android.TensorFlowInferenceInterface
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

private const val ENABLE_LOG_STATS = false

class ImageClassifier(
        private val inputName: String,
        private val outputName: String,
        private val imageSize: Long,
        private val labels: List<String>,
        private val imageBitmapPixels: IntArray,
        private val imageNormalizedPixels: FloatArray,
        private val floatResults: FloatArray,
        private val outputLocations: FloatArray,
        private val outputClasses: FloatArray,
        private var tensorFlowInference: TensorFlowInferenceInterface,
        private val path: String
) : Classifier {

    private val rectangleList: MutableList<Rectangle> = mutableListOf()

    override fun recognizeImage(bitmap: Bitmap): Result {
        preprocessImageToNormalizedFloats(bitmap)
        val outputQueue = getResults()
        return outputQueue.poll()
    }

    //Classifier uses floats not ints
    //algorithm from TensorFlow example for image normalize
    private fun preprocessImageToNormalizedFloats(bitmap: Bitmap) {
        // Preprocess the image data from 0-255 int to normalized float based
        // on the provided parameters.

//        val paths = Paths.get("file:///android_asset/graph.pb")
        val graphDef = Files.readAllBytes(File("file:///android_asset/graph.pb").toPath())
        val graph = Graph()
        graph.importGraphDef(graphDef)

        graph.operation("final_result")
        graph.operation("detection_boxes")
        graph.operation("detection_classes")
        tensorFlowInference = TensorFlowInferenceInterface(graph)

        val imageMean = 128
        val imageStd = 128.0f
        bitmap.getPixels(imageBitmapPixels, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        for (i in imageBitmapPixels.indices) {
            val `val` = imageBitmapPixels[i]
            imageNormalizedPixels[i * 3 + 0] = ((`val` shr 16 and 0xFF) - imageMean) / imageStd
            imageNormalizedPixels[i * 3 + 1] = ((`val` shr 8 and 0xFF) - imageMean) / imageStd
            imageNormalizedPixels[i * 3 + 2] = ((`val` and 0xFF) - imageMean) / imageStd
        }

        //Api from tensorflow, it takes asset manager, and the graph file path to load the graph
        //(input name, array with normalized values, dimensions of the input)
        //feed the classifier
        tensorFlowInference.feed(inputName, imageNormalizedPixels,
                1L, imageSize, imageSize, COLOR_CHANNELS.toLong())
        tensorFlowInference.run(arrayOf(outputName, "detection_boxes", "detection_classes"), ENABLE_LOG_STATS)
        //get the floatResults, and probabilities of two labels
        tensorFlowInference.fetch(outputName, floatResults)
        tensorFlowInference.fetch("detection_boxes", outputLocations)
        tensorFlowInference.fetch("detection_classes", outputClasses)

        for (i in floatResults.indices) {
            val xMin = outputLocations[4 * i + 1] * bitmap.width
            val yMin = outputLocations[4 * i] * bitmap.height
            val xMax = outputLocations[4 * i + 3] * bitmap.width
            val yMax = outputLocations[4 * i + 2] * bitmap.height

            rectangleList.add(Rectangle(xMin, yMin, xMax, yMax))
        }
    }

    //List of labels and coincidence
    //PriorityQueue will be filled with floatResults and poll with the one with highest coincidence
    private fun getResults(): PriorityQueue<Result> {
        val outputQueue = createOutputQueue()
        floatResults.indices.mapTo(outputQueue) { Result(labels[it], floatResults[it], rectangleList[it]) }
        return outputQueue
    }

    private fun createOutputQueue(): PriorityQueue<Result> {
        return PriorityQueue(
                labels.size,
                Comparator { (_, rConfidence), (_, lConfidence) ->
                    java.lang.Float.compare(lConfidence, rConfidence)
                })
    }
}

data class Rectangle(val xMin: Float, val yMin: Float, val xMax: Float, val yMax: Float)