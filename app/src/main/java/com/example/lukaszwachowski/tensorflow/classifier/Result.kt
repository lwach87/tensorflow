package com.example.lukaszwachowski.tensorflow.classifier

import com.example.lukaszwachowski.tensorflow.classifier.tensorflow.Rectangle

data class Result(val result: String, val confidence: Float, val rectangle: Rectangle)