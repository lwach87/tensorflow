package com.example.lukaszwachowski.tensorflow.classifier.tensorflow

import android.content.res.AssetManager
import com.example.lukaszwachowski.tensorflow.classifier.COLOR_CHANNELS
import com.example.lukaszwachowski.tensorflow.classifier.Classifier
import com.example.lukaszwachowski.tensorflow.classifier.MAX_VALUES
import com.example.lukaszwachowski.tensorflow.utils.getLabels
import org.tensorflow.contrib.android.TensorFlowInferenceInterface

object ImageClassifierFactory {

    fun create(
            assetManager: AssetManager,
            graphFilePath: String,
            labelsFilePath: String,
            imageSize: Int,
            inputName: String,
            outputName: String
    ): Classifier {

        //we get hot/not labels from assetManager
        val labels = getLabels(assetManager, labelsFilePath)

        return ImageClassifier(
                inputName,
                outputName,
                imageSize.toLong(),
                labels,
                IntArray(imageSize * imageSize),    //image bitmap pixels
                FloatArray(imageSize * imageSize * COLOR_CHANNELS), //image normalized pixels
                FloatArray(labels.size),    //results
                FloatArray(MAX_VALUES * 4),
                FloatArray(MAX_VALUES),
                TensorFlowInferenceInterface(assetManager, graphFilePath),
                graphFilePath
        )
    }
}