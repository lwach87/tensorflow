package com.example.lukaszwachowski.tensorflow.classifier

const val GRAPH_FILE_PATH = "file:///android_asset/graph.pb"
const val LABELS_FILE_PATH = "file:///android_asset/labels.txt"

const val GRAPH_INPUT_NAME = "Mul"
const val GRAPH_OUTPUT_NAME = "final_result"

const val IMAGE_SIZE = 299
const val COLOR_CHANNELS = 3

const val MAX_VALUES = 1001

const val ASSETS_PATH = "file:///android_asset/"